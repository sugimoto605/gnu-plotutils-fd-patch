/* This file is part of the GNU plotutils package.  Copyright (C) 1995,
   1996, 1997, 1998, 1999, 2000, 2005, 2008, Free Software Foundation, Inc.

   The GNU plotutils package is free software.  You may redistribute it
   and/or modify it under the terms of the GNU General Public License as
   published by the Free Software foundation; either version 2, or (at your
   option) any later version.

   The GNU plotutils package is distributed in the hope that it will be
   useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with the GNU plotutils package; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin St., Fifth Floor,
   Boston, MA 02110-1301, USA. */

/* This prints a single-font, single-font-size label.  When this is called,
   the current point is on the intended baseline of the label.  */

/* This version is for SVGPlotters.  We use the ISO-Latin-1 encoding, so <
   > & and left-quote and double-quote are the only characters we need to
   escape. */

#include "sys-defines.h"
#include "extern.h"

/* maximum length we support */
#define PL_MAX_SVG_STRING_LEN 256

/* The fixed value we specify for the font-size parameter, when any font is
   retrieved, in terms of `px'.  (We now scale as needed by choosing an
   appropriate transformation matrix.)  According to the SVG Authoring
   Guide, a `px' means simply a user-space unit, but some SVG renderers
   (e.g., in Firefox) get confused if it's smaller than 1.0 or so, and
   return absurdly scaled fonts.  Maybe they think px stands for pixels?
   :-) */
#define PL_SVG_FONT_SIZE_IN_PX 20.0

/* forward references */
static int write_svg_text_style (plOutbuf *page, const plDrawState *drawstate, int h_just, int v_just);

typedef struct
{
  char c;
  const char *s;
}
plCharEscape;

struct uSymbol_struct {
	char *cp;		/*	Unicode CodePoint	*/
	unsigned int ascii;	/*	ASCII			*/
};

static struct uSymbol_struct _uSymbol[] = {
	{"\u00A0",0x20},	/* NO-BREAK SPACE	# space*/
	/*{"\u0021",0x21},*/	/* EXCLAMATION MARK	# exclam*/
	{"\u2200",0x22},	/* FOR ALL	# universal*/
	/*{"\u0023",0x23},*/	/* NUMBER SIGN	# numbersign*/
	{"\u2203",0x24},	/* THERE EXISTS	# existential*/
	/*{"\u0025",0x25},*/	/* PERCENT SIGN	# percent*/
	/*{"\u0026",0x26}*/	/* AMPERSAND	# ampersand*/
	{"\u220B",0x27},	/* CONTAINS AS MEMBER	# suchthat*/
	/*{"\u0028",0x28},*/	/* LEFT PARENTHESIS	# parenleft*/
	/*{"\u0029",0x29},*/	/* RIGHT PARENTHESIS	# parenright*/
	{"\u2217",0x2A},	/* ASTERISK OPERATOR	# asteriskmath*/
	/*{"\u002B",0x2B},*/	/* PLUS SIGN	# plus*/
	/*{"\u002C",0x2C},*/	/* COMMA	# comma*/
	{"\u2212",0x2D},	/* MINUS SIGN	# minus*/
	/*{"\u002E",0x2E},*/	/* FULL STOP	# period*/
	/*{"\u002F",0x2F},*/	/* SOLIDUS	# slash*/
	/*{"\u0030",0x30},*/	/* DIGIT ZERO	# zero*/
	/*{"\u0031",0x31},*/	/* DIGIT ONE	# one*/
	/*{"\u0032",0x32},*/	/* DIGIT TWO	# two*/
	/*{"\u0033",0x33},*/	/* DIGIT THREE	# three*/
	/*{"\u0034",0x34},*/	/* DIGIT FOUR	# four*/
	/*{"\u0035",0x35},*/	/* DIGIT FIVE	# five*/
	/*{"\u0036",0x36},*/	/* DIGIT SIX	# six*/
	/*{"\u0037",0x37},*/	/* DIGIT SEVEN	# seven*/
	/*{"\u0038",0x38},*/	/* DIGIT EIGHT	# eight*/
	/*{"\u0039",0x39},*/	/* DIGIT NINE	# nine*/
	/*{"\u003A",0x3A},*/	/* COLON	# colon*/
	/*{"\u003B",0x3B},*/	/* SEMICOLON	# semicolon*/
	/*{"\u003C",0x3C},*/	/* LESS-THAN SIGN	# less*/
	/*{"\u003D",0x3D},*/	/* EQUALS SIGN	# equal*/
	/*{"\u003E",0x3E},*/	/* GREATER-THAN SIGN	# greater*/
	/*{"\u003F",0x3F},*/	/* QUESTION MARK	# question*/
	{"\u2245",0x40},	/* APPROXIMATELY EQUAL TO	# congruent*/
	{"\u0391",0x41},	/* GREEK CAPITAL LETTER ALPHA	# Alpha*/
	{"\u0392",0x42},	/* GREEK CAPITAL LETTER BETA	# Beta*/
	{"\u03A7",0x43},	/* GREEK CAPITAL LETTER CHI	# Chi*/
	{"\u0394",0x44},	/* GREEK CAPITAL LETTER DELTA	# Delta*/
	{"\u2206",0x44},	/* INCREMENT	# Delta*/
	{"\u0395",0x45},	/* GREEK CAPITAL LETTER EPSILON	# Epsilon*/
	{"\u03A6",0x46},	/* GREEK CAPITAL LETTER PHI	# Phi*/
	{"\u0393",0x47},	/* GREEK CAPITAL LETTER GAMMA	# Gamma*/
	{"\u0397",0x48},	/* GREEK CAPITAL LETTER ETA	# Eta*/
	{"\u0399",0x49},	/* GREEK CAPITAL LETTER IOTA	# Iota*/
	{"\u03D1",0x4A},	/* GREEK THETA SYMBOL	# theta1*/
	{"\u039A",0x4B},	/* GREEK CAPITAL LETTER KAPPA	# Kappa*/
	{"\u039B",0x4C},	/* GREEK CAPITAL LETTER LAMDA	# Lambda*/
	{"\u039C",0x4D},	/* GREEK CAPITAL LETTER MU	# Mu*/
	{"\u039D",0x4E},	/* GREEK CAPITAL LETTER NU	# Nu*/
	{"\u039F",0x4F},	/* GREEK CAPITAL LETTER OMICRON	# Omicron*/
	{"\u03A0",0x50},	/* GREEK CAPITAL LETTER PI	# Pi*/
	{"\u0398",0x51},	/* GREEK CAPITAL LETTER THETA	# Theta*/
	{"\u03A1",0x52},	/* GREEK CAPITAL LETTER RHO	# Rho*/
	{"\u03A3",0x53},	/* GREEK CAPITAL LETTER SIGMA	# Sigma*/
	{"\u03A4",0x54},	/* GREEK CAPITAL LETTER TAU	# Tau*/
	{"\u03A5",0x55},	/* GREEK CAPITAL LETTER UPSILON	# Upsilon*/
	{"\u03C2",0x56},	/* GREEK SMALL LETTER FINAL SIGMA	# sigma1*/
	{"\u03A9",0x57},	/* GREEK CAPITAL LETTER OMEGA	# Omega*/
	{"\u2126",0x57},	/* OHM SIGN	# Omega*/
	{"\u039E",0x58},	/* GREEK CAPITAL LETTER XI	# Xi*/
	{"\u03A8",0x59},	/* GREEK CAPITAL LETTER PSI	# Psi*/
	{"\u0396",0x5A},	/* GREEK CAPITAL LETTER ZETA	# Zeta*/
	/*{"\u005B",0x5B},*/	/* LEFT SQUARE BRACKET	# bracketleft*/
	{"\u2234",0x5C},	/* THEREFORE	# therefore*/
	/*{"\u005D",0x5D},*/	/* RIGHT SQUARE BRACKET	# bracketright*/
	{"\u22A5",0x5E},	/* UP TACK	# perpendicular*/
	/*{"\u005F",0x5F},*/	/* LOW LINE	# underscore*/
	{"\uF8E5",0x60},	/* RADICAL EXTENDER	# radicalex (CUS)*/
	{"\u03B1",0x61},	/* GREEK SMALL LETTER ALPHA	# alpha*/
	{"\u03B2",0x62},	/* GREEK SMALL LETTER BETA	# beta*/
	{"\u03C7",0x63},	/* GREEK SMALL LETTER CHI	# chi*/
	{"\u03B4",0x64},	/* GREEK SMALL LETTER DELTA	# delta*/
	{"\u03B5",0x65},	/* GREEK SMALL LETTER EPSILON	# epsilon*/
	{"\u03C6",0x66},	/* GREEK SMALL LETTER PHI	# phi*/
	{"\u03B3",0x67},	/* GREEK SMALL LETTER GAMMA	# gamma*/
	{"\u03B7",0x68},	/* GREEK SMALL LETTER ETA	# eta*/
	{"\u03B9",0x69},	/* GREEK SMALL LETTER IOTA	# iota*/
	{"\u03D5",0x6A},	/* GREEK PHI SYMBOL	# phi1*/
	{"\u03BA",0x6B},	/* GREEK SMALL LETTER KAPPA	# kappa*/
	{"\u03BB",0x6C},	/* GREEK SMALL LETTER LAMDA	# lambda*/
	{"\u00B5",0x6D},	/* MICRO SIGN	# mu*/
	{"\u03BC",0x6D},	/* GREEK SMALL LETTER MU	# mu*/
	{"\u03BD",0x6E},	/* GREEK SMALL LETTER NU	# nu*/
	{"\u03BF",0x6F},	/* GREEK SMALL LETTER OMICRON	# omicron*/
	{"\u03C0",0x70},	/* GREEK SMALL LETTER PI	# pi*/
	{"\u03B8",0x71},	/* GREEK SMALL LETTER THETA	# theta*/
	{"\u03C1",0x72},	/* GREEK SMALL LETTER RHO	# rho*/
	{"\u03C3",0x73},	/* GREEK SMALL LETTER SIGMA	# sigma*/
	{"\u03C4",0x74},	/* GREEK SMALL LETTER TAU	# tau*/
	{"\u03C5",0x75},	/* GREEK SMALL LETTER UPSILON	# upsilon*/
	{"\u03D6",0x76},	/* GREEK PI SYMBOL	# omega1*/
	{"\u03C9",0x77},	/* GREEK SMALL LETTER OMEGA	# omega*/
	{"\u03BE",0x78},	/* GREEK SMALL LETTER XI	# xi*/
	{"\u03C8",0x79},	/* GREEK SMALL LETTER PSI	# psi*/
	{"\u03B6",0x7A},	/* GREEK SMALL LETTER ZETA	# zeta*/
	/*{"\u007B",0x7B},*/	/* LEFT CURLY BRACKET	# braceleft*/
	/*{"\u007C",0x7C},*/	/* VERTICAL LINE	# bar*/
	/*{"\u007D",0x7D},*/	/* RIGHT CURLY BRACKET	# braceright*/
	{"\u223C",0x7E},	/* TILDE OPERATOR	# similar*/
	{"\u20AC",0xA0},	/* EURO SIGN	# Euro*/
	{"\u03D2",0xA1},	/* GREEK UPSILON WITH HOOK SYMBOL	# Upsilon1*/
	{"\u2032",0xA2},	/* PRIME	# minute*/
	{"\u2264",0xA3},	/* LESS-THAN OR EQUAL TO	# lessequal*/
	{"\u2044",0xA4},	/* FRACTION SLASH	# fraction*/
	{"\u2215",0xA4},	/* DIVISION SLASH	# fraction*/
	{"\u221E",0xA5},	/* INFINITY	# infinity*/
	{"\u0192",0xA6},	/* LATIN SMALL LETTER F WITH HOOK	# florin*/
	{"\u2663",0xA7},	/* BLACK CLUB SUIT	# club*/
	{"\u2666",0xA8},	/* BLACK DIAMOND SUIT	# diamond*/
	{"\u2665",0xA9},	/* BLACK HEART SUIT	# heart*/
	{"\u2660",0xAA},	/* BLACK SPADE SUIT	# spade*/
	{"\u2194",0xAB},	/* LEFT RIGHT ARROW	# arrowboth*/
	{"\u2190",0xAC},	/* LEFTWARDS ARROW	# arrowleft*/
	{"\u2191",0xAD},	/* UPWARDS ARROW	# arrowup*/
	{"\u2192",0xAE},	/* RIGHTWARDS ARROW	# arrowright*/
	{"\u2193",0xAF},	/* DOWNWARDS ARROW	# arrowdown*/
	{"\u00B0",0xB0},	/* DEGREE SIGN	# degree*/
	{"\u00B1",0xB1},	/* PLUS-MINUS SIGN	# plusminus*/
	{"\u2033",0xB2},	/* DOUBLE PRIME	# second*/
	{"\u2265",0xB3},	/* GREATER-THAN OR EQUAL TO	# greaterequal*/
	{"\u00D7",0xB4},	/* MULTIPLICATION SIGN	# multiply*/
	{"\u221D",0xB5},	/* PROPORTIONAL TO	# proportional*/
	{"\u2202",0xB6},	/* PARTIAL DIFFERENTIAL	# partialdiff*/
	{"\u2022",0xB7},	/* BULLET	# bullet*/
	{"\u00F7",0xB8},	/* DIVISION SIGN	# divide*/
	{"\u2260",0xB9},	/* NOT EQUAL TO	# notequal*/
	{"\u2261",0xBA},	/* IDENTICAL TO	# equivalence*/
	{"\u2248",0xBB},	/* ALMOST EQUAL TO	# approxequal*/
	{"\u2026",0xBC},	/* HORIZONTAL ELLIPSIS	# ellipsis*/
	{"\uF8E6",0xBD},	/* VERTICAL ARROW EXTENDER	# arrowvertex (CUS)*/
	{"\uF8E7",0xBE},	/* HORIZONTAL ARROW EXTENDER	# arrowhorizex (CUS)*/
	{"\u21B5",0xBF},	/* DOWNWARDS ARROW WITH CORNER LEFTWARDS	# carriagereturn*/
	{"\u2135",0xC0},	/* ALEF SYMBOL	# aleph*/
	{"\u2111",0xC1},	/* BLACK-LETTER CAPITAL I	# Ifraktur*/
	{"\u211C",0xC2},	/* BLACK-LETTER CAPITAL R	# Rfraktur*/
	{"\u2118",0xC3},	/* SCRIPT CAPITAL P	# weierstrass*/
	{"\u2297",0xC4},	/* CIRCLED TIMES	# circlemultiply*/
	{"\u2295",0xC5},	/* CIRCLED PLUS	# circleplus*/
	{"\u2205",0xC6},	/* EMPTY SET	# emptyset*/
	{"\u2229",0xC7},	/* INTERSECTION	# intersection*/
	{"\u222A",0xC8},	/* UNION	# union*/
	{"\u2283",0xC9},	/* SUPERSET OF	# propersuperset*/
	{"\u2287",0xCA},	/* SUPERSET OF OR EQUAL TO	# reflexsuperset*/
	{"\u2284",0xCB},	/* NOT A SUBSET OF	# notsubset*/
	{"\u2282",0xCC},	/* SUBSET OF	# propersubset*/
	{"\u2286",0xCD},	/* SUBSET OF OR EQUAL TO	# reflexsubset*/
	{"\u2208",0xCE},	/* ELEMENT OF	# element*/
	{"\u2209",0xCF},	/* NOT AN ELEMENT OF	# notelement*/
	{"\u2220",0xD0},	/* ANGLE	# angle*/
	{"\u2207",0xD1},	/* NABLA	# gradient*/
	{"\uF6DA",0xD2},	/* REGISTERED SIGN SERIF	# registerserif (CUS)*/
	{"\uF6D9",0xD3},	/* COPYRIGHT SIGN SERIF	# copyrightserif (CUS)*/
	{"\uF6DB",0xD4},	/* TRADE MARK SIGN SERIF	# trademarkserif (CUS)*/
	{"\u220F",0xD5},	/* N-ARY PRODUCT	# product*/
	{"\u221A",0xD6},	/* SQUARE ROOT	# radical*/
	{"\u22C5",0xD7},	/* DOT OPERATOR	# dotmath*/
	{"\u00AC",0xD8},	/* NOT SIGN	# logicalnot*/
	{"\u2227",0xD9},	/* LOGICAL AND	# logicaland*/
	{"\u2228",0xDA},	/* LOGICAL OR	# logicalor*/
	{"\u21D4",0xDB},	/* LEFT RIGHT DOUBLE ARROW	# arrowdblboth*/
	{"\u21D0",0xDC},	/* LEFTWARDS DOUBLE ARROW	# arrowdblleft*/
	{"\u21D1",0xDD},	/* UPWARDS DOUBLE ARROW	# arrowdblup*/
	{"\u21D2",0xDE},	/* RIGHTWARDS DOUBLE ARROW	# arrowdblright*/
	{"\u21D3",0xDF},	/* DOWNWARDS DOUBLE ARROW	# arrowdbldown*/
	{"\u25CA",0xE0},	/* LOZENGE	# lozenge*/
	{"\u2329",0xE1},	/* LEFT-POINTING ANGLE BRACKET	# angleleft*/
	{"\uF8E8",0xE2},	/* REGISTERED SIGN SANS SERIF	# registersans (CUS)*/
	{"\uF8E9",0xE3},	/* COPYRIGHT SIGN SANS SERIF	# copyrightsans (CUS)*/
	{"\uF8EA",0xE4},	/* TRADE MARK SIGN SANS SERIF	# trademarksans (CUS)*/
	{"\u2211",0xE5},	/* N-ARY SUMMATION	# summation*/
	{"\uF8EB",0xE6},	/* LEFT PAREN TOP	# parenlefttp (CUS)*/
	{"\uF8EC",0xE7},	/* LEFT PAREN EXTENDER	# parenleftex (CUS)*/
	{"\uF8ED",0xE8},	/* LEFT PAREN BOTTOM	# parenleftbt (CUS)*/
	{"\uF8EE",0xE9},	/* LEFT SQUARE BRACKET TOP	# bracketlefttp (CUS)*/
	{"\uF8EF",0xEA},	/* LEFT SQUARE BRACKET EXTENDER	# bracketleftex (CUS)*/
	{"\uF8F0",0xEB},	/* LEFT SQUARE BRACKET BOTTOM	# bracketleftbt (CUS)*/
	{"\uF8F1",0xEC},	/* LEFT CURLY BRACKET TOP	# bracelefttp (CUS)*/
	{"\uF8F2",0xED},	/* LEFT CURLY BRACKET MID	# braceleftmid (CUS)*/
	{"\uF8F3",0xEE},	/* LEFT CURLY BRACKET BOTTOM	# braceleftbt (CUS)*/
	{"\uF8F4",0xEF},	/* CURLY BRACKET EXTENDER	# braceex (CUS)*/
	{"\u232A",0xF1},	/* RIGHT-POINTING ANGLE BRACKET	# angleright*/
	{"\u222B",0xF2},	/* INTEGRAL	# integral*/
	{"\u2320",0xF3},	/* TOP HALF INTEGRAL	# integraltp*/
	{"\uF8F5",0xF4},	/* INTEGRAL EXTENDER	# integralex (CUS)*/
	{"\u2321",0xF5},	/* BOTTOM HALF INTEGRAL	# integralbt*/
	{"\uF8F6",0xF6},	/* RIGHT PAREN TOP	# parenrighttp (CUS)*/
	{"\uF8F7",0xF7},	/* RIGHT PAREN EXTENDER	# parenrightex (CUS)*/
	{"\uF8F8",0xF8},	/* RIGHT PAREN BOTTOM	# parenrightbt (CUS)*/
	{"\uF8F9",0xF9},	/* RIGHT SQUARE BRACKET TOP	# bracketrighttp (CUS)*/
	{"\uF8FA",0xFA},	/* RIGHT SQUARE BRACKET EXTENDER	# bracketrightex (CUS)*/
	{"\uF8FB",0xFB},	/* RIGHT SQUARE BRACKET BOTTOM	# bracketrightbt (CUS)*/
	{"\uF8FC",0xFC},	/* RIGHT CURLY BRACKET TOP	# bracerighttp (CUS)*/
	{"\uF8FD",0xFD},	/* RIGHT CURLY BRACKET MID	# bracerightmid (CUS)*/
	{"\uF8FE",0xFE}		/* RIGHT CURLY BRACKET BOTTOM	# bracerightbt (CUS)*/
};

unsigned char * _uSymbol_get(unsigned char *t){
	int USL=sizeof(_uSymbol)/sizeof(_uSymbol[0]);
	int i;
	for(i=0;i<USL;i++)
	{
	if (_uSymbol[i].ascii == *t)
		return (unsigned char *)_uSymbol[i].cp;
	}
	return t;
};

#define NUM_SVG_CHAR_ESCAPES 5
static const plCharEscape _svg_char_escapes[NUM_SVG_CHAR_ESCAPES] =
{
  {'\'', "apos"},
  {'"', "quot"},
  {'&', "amp"},
  {'<', "lt"},
  {'>', "gt"}
};
#define MAX_SVG_CHAR_ESCAPE_LEN 4 /* i.e., length of "apos" or "quot" */

/* SVG horizontal alignment styles, i.e., text-anchor attribute, indexed by
   internal number (left/center/right) */
static const char * const svg_horizontal_alignment_style[PL_NUM_HORIZ_JUST_TYPES] =
{ "start", "middle", "end" };

/* SVG vertical alignment styles, i.e., alignment-baseline attribute,
   indexed by internal number (top/half/base/bottom/cap) */
static const char * const svg_vertical_alignment_style[PL_NUM_VERT_JUST_TYPES] =
{ "text-before-edge", "central", "alphabetic", "text-after-edge", "hanging" };
/* This version of the paint_text_string method, for SVG Plotters, supports
   each of libplot's possible vertical justifications (see the list
   immediately above).  However, only the `baseline' justification is
   currently used.  That's because in s_defplot.c we set the Plotter
   parameter `have_vertical_justification' to false.  Too many SVG
   renderers don't support the SVG alignment-baseline attribute; e.g.,
   Firefox 1.5 doesn't.  So it's best for libplot to do its own vertical
   positioning of text strings. */

double
_pl_s_paint_text_string (R___(Plotter *_plotter) const unsigned char *s, int h_just, int v_just)
{
  const unsigned char *sp = s;
  unsigned char *t, *tp;
  unsigned char *u, *up;
  int i, n = 0;
  double local_matrix[6];
  double angle = _plotter->drawstate->text_rotation;
  int isText;
  
  /* replace certain printable ASCII characters by entities */
  tp = t = (unsigned char *)_pl_xmalloc ((2 + MAX_SVG_CHAR_ESCAPE_LEN) * strlen ((const char *)s) + 1);
  while (*sp && n < PL_MAX_SVG_STRING_LEN)
    {
      bool matched;
      int i;
      
      matched = false;
      for (i = 0; i < NUM_SVG_CHAR_ESCAPES; i++)
	{
	  if (*sp == (unsigned char)_svg_char_escapes[i].c)
	    {
	      matched = true;
	      break;
	    }
	}
      if (matched)
	{
	  *tp++ = (unsigned char)'&';
	  strcpy ((char *)tp, _svg_char_escapes[i].s);
	  tp += strlen (_svg_char_escapes[i].s);
	  *tp++ = (unsigned char)';';
	}
      else
	*tp++ = *sp;

      sp++;
      n++;
    }
  *tp = '\0';
  
  sprintf (_plotter->data->page->point, "<text ");
  _update_buffer (_plotter->data->page);

  /* CTM equals CTM_local * CTM_base, if matrix multiplication is defined
     as in PS and libplot. (Which is the opposite of the SVG convention,
     since SVG documentation uses column vectors instead of row vectors, so
     that the CTM is effectively transposed.  Although SVG's matrix()
     construct uses PS order for the six matrix elements... go figure.)

     Here CTM_local rotates by the libplot's text angle parameter, and
     translates to the correct position.  And CTM_base is libplot's current
     user_to_ndc transformation matrix.  We separate them because we use
     the CTM of the first-plotted object on the page as the page's global
     transformation matrix, and if that object happens to be a text object,
     we'd like it to simply to be the current user_to_ndc transformation
     matrix, i.e. not to include irrelevancies such as the text position
     and angle.

     Sigh... If only things were so simple.  SVG's native coordinate frame,
     which libplot's user coordinates must ultimately be mapped to,
     unfortunately uses a flipped-y convention, unlike PS and libplot.
     (The global flipping of y, relative to libplot's NDC coordinates, is
     accomplished by a scale(1,-1) that's placed at the head of the SVG
     file; see s_output.c.)  This flipping has a special effect on the
     drawing of text strings, though no other libplot primitive.  For
     everything to work out when drawing a text string, we must precede the
     sequence of transformations leading from user coordinates to native
     SVG coordinates by an initial scale(1,-1).  CTM_local, as defined
     above, must have two elements sign-flipped (see below).  Trust me. */

  local_matrix[0] = cos (M_PI * angle / 180.0);
  local_matrix[1] = sin (M_PI * angle / 180.0);
  local_matrix[2] = -sin (M_PI * angle / 180.0) * (-1);	/* SEE ABOVE */
  local_matrix[3] = cos (M_PI * angle / 180.0) * (-1); /* SEE ABOVE */

  /* since we now specify a fixed font-size, equal to PL_SVG_FONT_SIZE_IN_PX
     (see below), rather than specifying a font size equal to the
     font size in user units, we must here scale the text string to
     the right size */
  for (i = 0; i < 4; i++)
    local_matrix[i] *= (_plotter->drawstate->font_size
			/ PL_SVG_FONT_SIZE_IN_PX);

  local_matrix[4] = _plotter->drawstate->pos.x;
  local_matrix[5] = _plotter->drawstate->pos.y;
  _pl_s_set_matrix (R___(_plotter) local_matrix); 

  /* H. Sugimoto: If isText != 0, we use UTF-8. */
  isText=write_svg_text_style (_plotter->data->page, _plotter->drawstate, 
			 h_just, v_just);
  sprintf (_plotter->data->page->point, ">");
  _update_buffer (_plotter->data->page);

  if (isText)
    sprintf (_plotter->data->page->point, "%s", (char *)t);
  else
    {
    n = 0;
    tp = t;
    up = u = (unsigned char *)_pl_xmalloc (2  * strlen ((const char *)t) + 1);
    while (*tp && n < PL_MAX_SVG_STRING_LEN)
      {
      unsigned char * ret=_uSymbol_get(tp);
      *up++=*ret;
      if (*ret!=*tp) *up++=*(++ret);
      n++;
      tp++;
      }
      *up = '\0';
      sprintf (_plotter->data->page->point, "%s", (char *)u);
      free (u);
    };
  _update_buffer (_plotter->data->page);
  
  sprintf (_plotter->data->page->point, "</text>\n");
  _update_buffer (_plotter->data->page);

  free (t);

  return _plotter->get_text_width (R___(_plotter) s);
}

static int
write_svg_text_style (plOutbuf *page, const plDrawState *drawstate, int h_just, int v_just)
{
  const char *ps_name, *css_family, *css_generic_family; /* last may be NULL */
  const char *css_style, *css_weight, *css_stretch;
  bool css_family_is_ps_name;
  char color_buf[8];		/* enough room for "#ffffff", incl. NUL */
  int retval=0;

  /* extract official PS font name, and CSS font specification, from master
     table of PS [or PCL] fonts, in g_fontdb.c */
  switch (drawstate->font_type)
    {
      int master_font_index;

    case PL_F_POSTSCRIPT:
      master_font_index =
	(_pl_g_ps_typeface_info[drawstate->typeface_index].fonts)[drawstate->font_index];
      ps_name = _pl_g_ps_font_info[master_font_index].ps_name;
      css_family = _pl_g_ps_font_info[master_font_index].css_family;
      css_generic_family = _pl_g_ps_font_info[master_font_index].css_generic_family;
      css_style = _pl_g_ps_font_info[master_font_index].css_style;
      css_weight = _pl_g_ps_font_info[master_font_index].css_weight;
      css_stretch = _pl_g_ps_font_info[master_font_index].css_stretch;

      /* flag this font as used */
      page->ps_font_used[master_font_index] = true;

      break;
    case PL_F_PCL:
      master_font_index =
	(_pl_g_pcl_typeface_info[drawstate->typeface_index].fonts)[drawstate->font_index];
      ps_name = _pl_g_pcl_font_info[master_font_index].ps_name;
      css_family = _pl_g_pcl_font_info[master_font_index].css_family;
      css_generic_family = _pl_g_pcl_font_info[master_font_index].css_generic_family;
      css_style = _pl_g_pcl_font_info[master_font_index].css_style;
      css_weight = _pl_g_pcl_font_info[master_font_index].css_weight;
      css_stretch = _pl_g_pcl_font_info[master_font_index].css_stretch;

      /* flag this font as used */
      page->pcl_font_used[master_font_index] = true;

      break;
    default:			/* shouldn't happen */
      return retval;
      break;
    }
  retval=strcmp(ps_name,"Symbol");

  if (strcmp (ps_name, css_family) == 0)
    /* no need to specify both */
    css_family_is_ps_name = true;
  else
    css_family_is_ps_name = false;

  /* N.B. In each of the following four sprintf()'s, we should apparently
     enclose css_family in single quotes, at least if it contains a space.
     But doing so would cause the SVG renderer in `display', which is part
     of the ImageMagick package, to reject the emitted SVG file. */

/*
  if (css_generic_family)
    {
      if (css_family_is_ps_name)
	sprintf (page->point, "font-family=\"%s,%s\" ",
		 css_family, css_generic_family);
      else
	sprintf (page->point, "font-family=\"%s,%s,%s\" ",
		 ps_name, css_family, css_generic_family);
    }
  else
    {
      if (css_family_is_ps_name)
	sprintf (page->point, "font-family=\"%s\" ",
		 css_family);
      else
	sprintf (page->point, "font-family=\"%s,%s\" ",
		 ps_name, css_family);
    }
*/
	sprintf (page->point, "font-family=\"%s\" ",
		 css_family);

  _update_buffer (page);
  
  if (strcmp (css_style, "normal") != 0) /* not default */
    {
      sprintf (page->point, "font-style=\"%s\" ",
	       css_style);
      _update_buffer (page);
    }

  if (strcmp (css_weight, "normal") != 0) /* not default */
    {
      sprintf (page->point, "font-weight=\"%s\" ",
	       css_weight);
      _update_buffer (page);
    }

  if (strcmp (css_stretch, "normal") != 0) /* not default */
    {
      sprintf (page->point, "font-stretch=\"%s\" ",
	       css_stretch);
      _update_buffer (page);
    }

  sprintf (page->point, "font-size=\"%.5gpx\" ",
	   /* see comments above for why we don't simply specify
	      drawstate->font_size here */
	   PL_SVG_FONT_SIZE_IN_PX);
  _update_buffer (page);

  if (h_just != PL_JUST_LEFT)	/* not default */
    {
      sprintf (page->point, "text-anchor=\"%s\" ",
	       svg_horizontal_alignment_style[h_just]);
      _update_buffer (page);
    }

  if (v_just != PL_JUST_BASE)	/* not default */
    {
      sprintf (page->point, "alignment-baseline=\"%s\" ",
	       svg_vertical_alignment_style[v_just]);
      _update_buffer (page);
    }

  /* currently, we never draw character outlines; we only fill */
  sprintf (page->point, "stroke=\"none\" ");
  _update_buffer (page);

  if (drawstate->pen_type)
    /* according to libplot convention, text should be filled, and since
       SVG's default filling is "none", we must say so */
    {
      sprintf (page->point, "fill=\"%s\" ",
	       _libplot_color_to_svg_color (drawstate->fgcolor, color_buf));
      _update_buffer (page);
    }
    return retval;
}
